# Docker image upgrade tool
# Copyright © 2019 Free Software Foundation Europe
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import dateutil.parser
import docker
import logging
from pytz import utc
import re
import requests
from time import sleep
from typing import Sequence
import argparse
import yaml

docker_client = docker.from_env()

config = configparser.ConfigParser()
config.read("config.cfg")

# https://try.gitea.io/api/swagger

GITEA_URL = config["GITEA"]["base_url"]
GITEA_TOKEN = config["GITEA"]["token"]
DRONE_URL = config["DRONE"]["base_url"]
DRONE_TOKEN = config["DRONE"]["token"]

logging.basicConfig(
    level=logging.INFO, format="%(asctime)-15s %(levelname)-6s %(message)s"
)
logger = logging.getLogger(__name__)


def get_base_image_name(
    repo_name: str,
    default_branch: str = "master",
    dockerfile_filename: str = "Dockerfile",
) -> str:
    """Get the Dockerfile and return the image found in the 'FROM' line

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :default_branch: The default git branch for the git repository
    :dockerfile_filename: The name of the Dockerfile. Defaults to 'Dockerfile'

    :returns: The image name
    """
    url = (
        "https://git.fsfe.org/api/v1/repos/"
        + repo_name
        + "/raw/"
        + default_branch
        + "/"
        + dockerfile_filename
        + "?access_token="
        + GITEA_TOKEN
    )
    response = requests.get(url)
    dockerfile = response.text
    if response.status_code == 404:
        logger.fatal(
            "Could not find Dockerfile '{0}' for {1}".format(
                dockerfile_filename, repo_name
            )
        )
        exit()

    return re.search(r"FROM\s+([/:.\w-]+)", dockerfile).groups()[0].split(":")


def get_docker_compose_image_name(
    repo_name: str, default_branch: str = "master"
) -> list:
    """Get the Dockerfile and return the image found in the 'FROM' line

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :default_branch: The default git branch for the git repository
    :dockerfile_filename: The name of the Dockerfile. Defaults to 'Dockerfile'

    :returns: The image names
    """
    url = (
        "https://git.fsfe.org/api/v1/repos/"
        + repo_name
        + "/raw/"
        + default_branch
        + "/docker-compose.yml?access_token="
        + GITEA_TOKEN
    )
    response = requests.get(url)
    docker_compose_file = response.text

    if response.status_code == 404:
        logger.warning("Could not find docker-compose,yml for {0}".format(repo_name))
        return []

    docker_compose = yaml.load(docker_compose_file, Loader=yaml.SafeLoader)

    # Filter out the images we're building locally. Only take the pulled images,
    # The images built locally will have a Dockerfile anyway
    return [
        service["image"].split(":")
        for service in docker_compose["services"].values()
        if "build" not in service
    ]


def find_dockerfile_filename(repo_name: str, default_branch: str) -> Sequence[str]:
    """Get the file names in the repository and return the ones that match
    ^Dockerfile

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :default_branch: The default git branch for the git repository

    :returns: The Dockerfile names
    """
    url = (
        "https://git.fsfe.org/api/v1/repos/"
        + repo_name
        + "/git/trees/"
        + default_branch
        + "?token="
        + GITEA_TOKEN
    )
    response = requests.get(url)
    dockerfiles = [
        file["path"]
        for file in response.json()["tree"]
        if file["path"].startswith("Dockerfile")
    ]

    return dockerfiles


def trigger_drone_build(repo_name: str, build_number: str) -> str:
    """Trigger a drone build meant to rebuild containers once the docker image
    has been updated

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :build_number: The build number to start

    :returns: The build number of the started build

    """
    build = requests.post(
        "https://drone.fsfe.org/api/repos/" + repo_name + "/builds/" + build_number,
        headers={"Authorization": "Bearer " + DRONE_TOKEN},
    ).json()

    return str(build["number"])


def get_last_build_number(repo_name: str, default_branch: str) -> str:
    """Get last build number for the default branch. This is used to trigger a new build

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :default_branch: The default git branch for the git repository

    :returns: The build number
    """
    last_builds = requests.get(
        "https://drone.fsfe.org/api/repos/" + repo_name + "/builds",
        headers={"Authorization": "Bearer " + DRONE_TOKEN},
    ).json()

    last_id = max(
        [build["number"] for build in last_builds if build["target"] == default_branch]
    )

    return str(last_id)


def get_build_status(repo_name: str, build_number: str) -> str:
    """Get the build status. Used to wait until a triggered build is complete.

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :build_number: The build number to start

    :returns: The build status
    """

    build = requests.get(
        "https://drone.fsfe.org/api/repos/" + repo_name + "/builds/" + build_number,
        headers={"Authorization": "Bearer " + DRONE_TOKEN},
    ).json()

    return build["status"]


def upgrade_repo(repo_name: str, default_branch: str):
    """Once the image has been upgraded, rebuild the containers by triggered a
    new drone build.

    :repo_name: The repo full name. e.g fsfe-system-hackers/community
    :default_branch: The default git branch for the git repository
    """
    # Get the last build number and run it again
    last_id = get_last_build_number(repo_name, default_branch)
    build_number = trigger_drone_build(repo["slug"], last_id)
    logger.info(
        "Triggered build {0}/{1}. Waiting for completion".format(
            repo_name, build_number
        )
    )
    build_status = get_build_status(repo_name, build_number)

    # Wait until the build ends
    while build_status != "success" and build_status != "failure":
        sleep(10)
        build_status = get_build_status(repo_name, build_number)
    logger.info("Build complete. Status: {0}".format(build_status))


if __name__ == "__main__":

    logger.info("Starting the docker image upgrade tool")

    arg_parser = argparse.ArgumentParser(description="Docker image upgrade tool")
    arg_parser.add_argument(
        "--repo",
        action="append",
        help="Repository for which you wish to upgrade the images (format <username>/<repo>). Use the argument multiple times for multiple repositories. If this argument is not specified, all repositories are considered",
    )

    args = arg_parser.parse_args()

    drone_repos = requests.get(
        "https://drone.fsfe.org/api/user/repos",
        headers={"Authorization": "Bearer " + DRONE_TOKEN},
    ).json()

    logger.info("Found {0} drone repositories".format(len(drone_repos)))

    # If at least one repository is specified on the command line
    if args.repo is not None:
        logger.info("Filtering to {0}".format(", ".join(args.repo)))
        # Filter out repositories not in the list specified on the command line
        drone_repos = list(filter(lambda repo: repo["slug"] in args.repo, drone_repos))
        # If the filtered list is not equal to the user-supplied list
        if len(args.repo) != len(drone_repos):
            logger.fatal(
                "At least one repository was not found: "
                + ", ".join(
                    set(args.repo)
                    - set([drone_repo["slug"] for drone_repo in drone_repos])
                )
            )

    for repo in drone_repos:
        logger.info("Processing {0}".format(repo["slug"]))
        docker_compose_images = get_docker_compose_image_name(
            repo["slug"], repo["default_branch"]
        )
        # Get the Dockerfiles found in the repo
        dockerfile_filenames = find_dockerfile_filename(
            repo["slug"], repo["default_branch"]
        )
        if len(dockerfile_filenames + docker_compose_images) == 0:
            logger.warning(
                "No Dockerfile and no docker compose files found for {0}".format(
                    repo["slug"]
                )
            )
            continue
        else:
            logger.info(
                "Found {0} Dockerfile(s) and {1} docker compose images".format(
                    len(dockerfile_filenames), len(docker_compose_images)
                )
            )

        docker_images = [
            get_base_image_name(repo["slug"], repo["default_branch"], dockerfile)
            for dockerfile in dockerfile_filenames
        ] + docker_compose_images

        for base_image_name in docker_images:
            # Get the local image
            local_image = docker_client.images.get(":".join(base_image_name))
            local_image_date = dateutil.parser.parse(
                local_image.attrs["Created"]
            ).astimezone(utc)
            logger.info(
                "Found local image {0} created on {1}".format(
                    ":".join(base_image_name), local_image.attrs["Created"]
                )
            )
            # Get the latest one
            pulled_image = docker_client.images.pull(
                base_image_name[0], tag=base_image_name[1]
            )
            pulled_image_date = dateutil.parser.parse(
                pulled_image.attrs["Created"]
            ).astimezone(utc)
            logger.info(
                "Found image {0} created on {1}".format(
                    ":".join(base_image_name), pulled_image.attrs["Created"]
                )
            )
            # Compare their dates
            if pulled_image_date > local_image_date:
                logger.warning(
                    "Newer image {0}. Upgrading...".format(":".join(base_image_name))
                )
                upgrade_repo(repo["slug"], repo["default_branch"])
            else:
                logger.info("The current image is already the latest one")
