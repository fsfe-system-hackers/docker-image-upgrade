# Docker image upgrade tool

The goal of this script is to make sure we use the latest available docker
image for all containers.

It works as follows:

1. Loop through git repositories where Drone CI is enabled
2. Get the `Dockerfile` of each git repository and get the docker base image
   (this is specified in the `FROM` LINE in the Dockerfiles)
3. Try to find the `docker-compose.yml` file and get images pulled from remote
   registries
4. Connect to the local docker daemon, pull the images and see if a newer
   version of them exist
5. If it does, trigger a CI (Continuous integration) job to start a new
   container on top of the new image

# How to use this?

Get the [latest](https://git.fsfe.org/fsfe-system-hackers/docker-image-upgrade/releases)
version (or clone the repository if you feel adventurous), rename
[`config.cfg.dist`](config.cfg.dist) to `config.cfg` and fill in the right
parameters. Then install the required Python packages found in
[requirements.txt](requirements.txt) using pyenv:

```bash
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Then you can run the script with `python upgrade-drone.py` to upgrade images for
all projects, or use the `--repo` command line flag to specify a git repository.
